from rest_framework import serializers
from .models import *


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ('vehicle_number','number_of_tyres')

class CustomerSerializer(serializers.ModelSerializer):
    my_vehicles = VehicleSerializer(many=True, read_only=True)
    class Meta:
        model = Customer
        fields = ('name','description','last_entry','my_vehicles')
