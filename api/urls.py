from django.urls import path, include
from . import views

urlpatterns = [
    path('customers', views.user_list),
    path('new_c',views.CustomerViewSet.as_view({'get': 'list'}))
]