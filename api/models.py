from django.db import models

# Create your models here.

class Customer(models.Model):
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=150)
    last_entry = models.DateTimeField()

    def __str__(self):
        return self.name    

class Vehicle(models.Model):
    vehicle_number = models.CharField(max_length=123)
    number_of_tyres = models.IntegerField(default=4)
    owner = models.ForeignKey(Customer, related_name='my_vehicles', on_delete=models.CASCADE)


    def __str__(self):
        return self.owner.name + " | " + self.vehicle_number
    