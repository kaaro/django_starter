from django.shortcuts import render
from django.http import JsonResponse
from .models import Customer
from rest_framework import viewsets
from .serializers import *
# Create your views here.


def user_list(request):
    response=[]

    for user in Customer.objects.all():
        response.append({ 'name': user.name, 'last_entry': str(user.last_entry)})
    
    return JsonResponse(response, safe=False)

    
class CustomerViewSet(viewsets.ModelViewSet):
    serializer_class=CustomerSerializer
    queryset = Customer.objects.all()